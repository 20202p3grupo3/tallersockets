all: bin/servidor bin/cliente

bin/servidor: obj/servidor.o
	gcc obj/servidor.o -o bin/servidor

bin/cliente: obj/cliente.o
	gcc obj/cliente.o -o bin/cliente

obj/servidor.o: src/servidor.c
	gcc -Wall -c src/servidor.c -I include/ -o obj/servidor.o

obj/cliente.o: src/cliente.c
	gcc -Wall -c src/cliente.c  -I include/ -o obj/cliente.o


.PHONY: clean
clean:
	rm bin/* obj/*









