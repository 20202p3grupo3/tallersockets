#include <sys/socket.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <string.h> 
#include <errno.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <fcntl.h> 
#define TAM_COLA_CONX 3


int main(int argc, char **argv){
        
        unsigned short puerto;
        struct sockaddr_in direccion;
	char opcion;
	//printf("Se recibieron %d argumentos\n", argc);
	//Los argumentos que no reciben argumentos (no seguidos de :),
	//Los pueden poner juntos -ix equivale a -i -x
	while( (opcion = getopt(argc, argv, "i:p:")) != -1 ){		//argumentos que toman argumento van seguidos de :
										//En este caso f toma argumento. El argumento siempre estara
										//en la variable optarg
		//procesamos argumentos
		
		switch(opcion){
			case 'i':
				direccion.sin_addr.s_addr = inet_addr(optarg);				
			case 'p':
				puerto = (unsigned short) atoi(optarg);				
			default:
				break;

		}
	}



        int sockfd = socket(AF_INET,SOCK_STREAM,0);
        if(sockfd < 0){
                perror("Error al crear el socket");
                exit(1);

        }
        
        direccion.sin_family = AF_INET;
        direccion.sin_port = htons(puerto);

        int res = bind(sockfd, (struct sockaddr *) &direccion, sizeof(direccion) );
        
        if(res < 0){
              perror("Fallo el bind");
              close(sockfd);
              exit(1);

       }
       
       res = listen(sockfd, TAM_COLA_CONX);
       if(res < 0){
              perror("Fallo listen");
              close(sockfd);
              exit(1);
       }

        while(1){

              int sockfd_conectado = accept(sockfd, NULL, 0);
              if(sockfd_conectado < 0){
                 perror("fallo accept()"); 
                 close(sockfd);
                 exit(1);
             }

       char buf[100] = {0};
       res = read(sockfd_conectado, buf, 100);
       if(res < 0){
          close(sockfd_conectado);
          exit(1);
       }
      
        //write(sockfd_conectado, "adios", 5);
        umask(0);
	printf("ruta: %s\n",buf);
        int fd = open(buf, O_RDONLY,0666);
        printf("fd: %d\n",fd);
	char buf2[1000]={0};
        int n = 0;
  	if(fd < 0){
              perror("Error al abrir el archivo");
              exit(-1);
        }
        while((n = read(fd, buf2, 1000)) != 0){
             if(n < 0){
                  perror("error al leer archivo");
                  close(sockfd_conectado);
                  close(fd);
                  break;
            }
            
            int w = write(sockfd_conectado, buf2 , n);
            if(w < 0){
                perror("fallo el envio del archivo");
                close(sockfd_conectado);
                close(fd);
                break;
            }


     }

  
        close(sockfd_conectado);
        }
        

        close(sockfd);
	return 0;
}

