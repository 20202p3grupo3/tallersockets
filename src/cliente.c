#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

int main(int argc,char **argv){
	int option;
	unsigned short puerto;
	struct sockaddr_in direccion;
	char *bufrt;
	char *buflcl;
	while ((option = getopt(argc, argv, "i:p:R:L:"))!=-1){
    		switch(option){
		case 'i': 
			direccion.sin_addr.s_addr = inet_addr(optarg);
		case 'p':
			puerto = (unsigned short) atoi(optarg);
			direccion.sin_port = htons(puerto);
			direccion.sin_family = AF_INET;
		case 'R':
			bufrt = optarg;
		case 'L':
			buflcl = optarg;
		}
	}
	int sockfd= socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd<0){
		perror("Error al crear socket");
		exit(1);
	}
	printf("sockfd= %d\n",sockfd);

	int res=connect(sockfd, (struct sockaddr *)&direccion,sizeof(direccion));
	if(res<0){
		perror("fallo el connect");
		close(sockfd);
		exit(1);
	}
	
	//enviar y recibir info
	res = write(sockfd, bufrt, strlen(bufrt));
	printf("ruta: %s\n",bufrt);
	if(res<0){
		perror("fallo el write");
		close(sockfd);
		exit(1);
	}

	char buf[1000]={0};
	/*res = read(sockfd,buf,1000);
        if(res < 0){
	  perror("fallo read");
	  close(sockfd);
          exit(1);
         }*/
          umask(0);
         // int fd = open(buf, O_RDONLY, 0666);
	  int destino = open(buflcl,O_TRUNC | O_CREAT | O_RDWR, 0666);
          printf("destino: %s\n", buflcl);
          if(destino < 0){
             perror("Error al abrir la ruta local");
             exit(-1);

          }
	  int bytesdnd;
          //int buf2[1000]={0};
          int n=0;
          while((n = read(sockfd, buf, 1000))!=0){
                 if(n < 0){
                    perror("error al leer archivo");
                    close(sockfd);
                    //close(fd);
                  }
	  bytesdnd = write(destino, buf, n);
          if(bytesdnd < 0){
		  perror("ERROR AL DESCARGAR EL ARCHIVO EN EL DESTINO");				
                  close(sockfd);
                  //close(fd);
           }
         
//close(fd);

}

close(sockfd);
return 0;
}
